package com.bankaya.challenge.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bankaya.challenge.entity.LogEntity;

/**
 * Inteface Log Repository.
 * @author Antonio Bravo.
 *
 */
@Repository
public interface LogRepository extends JpaRepository<LogEntity, Long> {

}
