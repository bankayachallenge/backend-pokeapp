package com.bankaya.challenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Clase principal PokeApp.
 * @author Antonio Bravo.
 *
 */
@SpringBootApplication
public class PokeApp1Application {

	/**
	 * Main.
	 * @param args Args.
	 */
	public static void main(String[] args) {
		SpringApplication.run(PokeApp1Application.class, args);
	}

}
