package com.bankaya.challenge.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Clase Generica.
 * @author Antonio Bravo.
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GenericObject {
	
	/** Name */
	private String name;
	/** URL */
	private String url;

}
