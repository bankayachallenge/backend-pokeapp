package com.bankaya.challenge.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Pokemon Ability.
 * @author Antonio Bravo.
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PokemonAbility {
	
	/** Ability */
	private GenericObject ability;
	/** Hidden */
	private boolean hidden;
	/** Slot */
	private int slot;

}
