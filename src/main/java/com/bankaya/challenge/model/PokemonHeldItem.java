package com.bankaya.challenge.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Pokemon Held Item.
 * @author Antonio Bravo.
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PokemonHeldItem {
	
	/** Item */
	private GenericObject item;
	/** Version Details */
	private List<VersionDetail> version_details;

}
