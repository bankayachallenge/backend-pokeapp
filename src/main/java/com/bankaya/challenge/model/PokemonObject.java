package com.bankaya.challenge.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Pokemon Object.
 * @author Antonio Bravo.
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PokemonObject {
	
	/** Abilities */
	private List<PokemonAbility> abilities;
	/** Base Experience */
	private int base_experience;
	/** Height */
	private int height;
	/** Held Items */
	private List<PokemonHeldItem> held_items;
	/** Id */
	private int id;
	/** Location Area Encounters */
	private String location_area_encounters;
	/** Name */
	private String name;
	/** Order */
	private int order;
	
	
}
