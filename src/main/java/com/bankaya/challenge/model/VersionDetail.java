package com.bankaya.challenge.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Version Detail.
 * @author Antonio Bravo.
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VersionDetail {
	
	/** Rarity */
	private int rarity;
	/** Version */
	private GenericObject version;

}
