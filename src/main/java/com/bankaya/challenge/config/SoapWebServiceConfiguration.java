package com.bankaya.challenge.config;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.SimpleWsdl11Definition;
import org.springframework.ws.wsdl.wsdl11.Wsdl11Definition;

/**
 * Configuracion Pokemon WS. 
 * @author Antonio Bravo.
 *
 */
@EnableWs
@Configuration
public class SoapWebServiceConfiguration extends WsConfigurerAdapter {
	
	/** Default timeout. */
    private static final int TIMEOUT_MILLISECONDS = 20000;

    /**
     * Dispacher Servlet.
     * @param applicationContext Context.
     * @return Dispacher Servlet.
     */
    @Bean
    ServletRegistrationBean<MessageDispatcherServlet> messageDispatcherServlet(ApplicationContext applicationContext) {
        MessageDispatcherServlet messageDispatcherServlet = new MessageDispatcherServlet();
        messageDispatcherServlet.setApplicationContext(applicationContext);
        return new ServletRegistrationBean<>(messageDispatcherServlet, "/ws/*");
    }

    /**
     * Wsdl Definition.
     * @return Wsdl Definition.
     */
    @Bean(name = "pokemonDemo")
    public Wsdl11Definition wsdl11Definition() {
        SimpleWsdl11Definition simpleWsdl11Definition = new SimpleWsdl11Definition();
        simpleWsdl11Definition.setWsdl(new ClassPathResource("/pokemon.wsdl"));
        return simpleWsdl11Definition;
    }
    


    /**
     * Bean de cliente rest.
     * @return  Un cliente rest de tipo {@link RestTemplate}
     */
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate(getClientHttpRequestFactory());
    }

    /**
     * Metodo de configuracion para timeout.
     * @return  Objeto {@link ClientHttpRequestFactory} con datos de configuracion de timeout.
     */
    private ClientHttpRequestFactory getClientHttpRequestFactory() {
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory
                                    = new HttpComponentsClientHttpRequestFactory();
        HttpClient httpClient = HttpClientBuilder.create().setRedirectStrategy(new LaxRedirectStrategy()).build();
        clientHttpRequestFactory.setHttpClient(httpClient);
        clientHttpRequestFactory.setConnectTimeout(TIMEOUT_MILLISECONDS);
        return clientHttpRequestFactory;
    }
}