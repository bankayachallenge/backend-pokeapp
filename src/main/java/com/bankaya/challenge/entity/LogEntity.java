package com.bankaya.challenge.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Clase Log Entity.
 * @author Antonio Bravo.
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "log")
public class LogEntity {
	
	/** Id */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	/** IP */
	@Column(name = "ip")
	private String ip;
	/** Fecha */
	@Column(name = "fecha")
	@Temporal(value=TemporalType.DATE)
	private Date fecha;
	/** Metodo */
	@Column(name = "metodo")
	private String metodo;

}
