package com.bankaya.challenge.endpoint;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.bankaya.challenge.entity.LogEntity;
import com.bankaya.challenge.model.PokemonAbility;
import com.bankaya.challenge.model.PokemonHeldItem;
import com.bankaya.challenge.repository.LogRepository;
import com.bankaya.challenge.service.PokemonService;
import com.bankaya.pokemon.types.pokemon.Abilities;
import com.bankaya.pokemon.types.pokemon.AbilitiesInput;
import com.bankaya.pokemon.types.pokemon.AbilitiesOutput;
import com.bankaya.pokemon.types.pokemon.BaseExperienceInput;
import com.bankaya.pokemon.types.pokemon.BaseExperienceOutput;
import com.bankaya.pokemon.types.pokemon.HeldItem;
import com.bankaya.pokemon.types.pokemon.HeldItemsInput;
import com.bankaya.pokemon.types.pokemon.HeldItemsOutput;
import com.bankaya.pokemon.types.pokemon.IdInput;
import com.bankaya.pokemon.types.pokemon.IdOutput;
import com.bankaya.pokemon.types.pokemon.Item;
import com.bankaya.pokemon.types.pokemon.LocalAreaEncountersInput;
import com.bankaya.pokemon.types.pokemon.LocalAreaEncountersOutput;
import com.bankaya.pokemon.types.pokemon.NameInput;
import com.bankaya.pokemon.types.pokemon.NameOutput;
import com.bankaya.pokemon.types.pokemon.ObjectFactory;
import com.bankaya.pokemon.types.pokemon.VersionDetail;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import lombok.extern.slf4j.Slf4j;

/**
 * Clase Endpoint Pokemon.
 * @author Antonio Bravo.
 *
 */
@Endpoint
@Slf4j
public class Pokemon {
	
	/** Method Held Items. */
	private static final String METHOD_HELD_ITEMS = "held_items";
	/** Method Location Area Encounters. */
	private static final String METHOD_LOCATION_AREA_ENCOUNTERS = "location_area_encounters";
	/** Method Id. */
	private static final String METHOD_ID = "id";
	/** Method Name. */
	private static final String METHOD_NAME = "name";
	/** Method Base Experience. */
	private static final String METHOD_BASE_EXPERIENCE = "base_experience";
	/** Method Abilities. */
	private static final String METHOD_ABILITIES = "abilities";
	/** Pokemon Service. */
	@Autowired
	private PokemonService service;
	/** Http Servlet Request. */
	@Autowired
	private HttpServletRequest request;
	/** Log Repository */
	@Autowired
	private LogRepository logRepository;
	
	/**
	 * Pokemon Abilities.
	 * @param input Pokemon nombre.
	 * @return Abilities.
	 */
	@ResponsePayload
    @PayloadRoot(namespace = "http://pokemon.bankaya.com/types/pokemon", localPart = "AbilitiesInput")
    public AbilitiesOutput abilities(@RequestPayload AbilitiesInput input) {
		log.info("--> Pokemon abilities");
		saveLog(METHOD_ABILITIES);
        ObjectFactory objectFactory = new ObjectFactory();
        AbilitiesOutput output = objectFactory.createAbilitiesOutput();
        try {
	        if (StringUtils.isNotBlank(input.getPokemon())) {
	        	log.info("----> " + input.getPokemon() + "!!!");
	        	List<PokemonAbility> abilities = service.getAbilities(input.getPokemon());
				log.info("----> Abilities: " + abilities);
				Type abilityListType = new TypeToken<List<Abilities>>(){}.getType();
		        List<Abilities> abilitiesOutput = output.getAbilities();
		        String abilitiesJson = new Gson().toJson(abilities);
		        log.info("----> AbilitiesJson: " + abilitiesJson);
		        List<Abilities> abilitiesOutputJson = new Gson().fromJson(abilitiesJson, abilityListType);
		        for(Abilities ability : abilitiesOutputJson) {
		        	abilitiesOutput.add(ability);
		        }
	        }
        } catch (Exception e) {
        	log.error("ERROR [Endpoint abilities]: " + e.getMessage());
			e.printStackTrace();
		}
        return output;
    }
	
	/**
	 * Pokemon Base Experience.
	 * @param input Pokemon nombre.
	 * @return Base experience.
	 */
	@ResponsePayload
    @PayloadRoot(namespace = "http://pokemon.bankaya.com/types/pokemon", localPart = "BaseExperienceInput")
	public BaseExperienceOutput baseExperience(@RequestPayload BaseExperienceInput input) {
		log.info("--> Pokemon Base Experience");
		saveLog(METHOD_BASE_EXPERIENCE);
        ObjectFactory objectFactory = new ObjectFactory();
        BaseExperienceOutput output = objectFactory.createBaseExperienceOutput();
        if (StringUtils.isNotBlank(input.getPokemon())) {
        	log.info("----> " + input.getPokemon() + "!!!");
        	int experience = service.getBaseExperience(input.getPokemon());
        	output.setBaseExperience(experience);
        }
        return output;
	}
	
	/**
	 * Pokemon Nombre.
	 * @param input Pokemon nombre.
	 * @return Nombre.
	 */
	@ResponsePayload
    @PayloadRoot(namespace = "http://pokemon.bankaya.com/types/pokemon", localPart = "NameInput")
	public NameOutput nombre(@RequestPayload NameInput input) {
		log.info("--> Pokemon Name");
		saveLog(METHOD_NAME);
        ObjectFactory objectFactory = new ObjectFactory();
        NameOutput output = objectFactory.createNameOutput();
        if (StringUtils.isNotBlank(input.getPokemon())) {
        	log.info("----> " + input.getPokemon() + "!!!");
        	String name = service.getName(input.getPokemon());
        	output.setName(name);
        }
        return output;
	}
	
	/**
	 * Pokemon Id.
	 * @param input Pokemon nombre.
	 * @return Id.
	 */
	@ResponsePayload
    @PayloadRoot(namespace = "http://pokemon.bankaya.com/types/pokemon", localPart = "IdInput")
	public IdOutput id(@RequestPayload IdInput input) {
		log.info("--> Pokemon Id");
		saveLog(METHOD_ID);
        ObjectFactory objectFactory = new ObjectFactory();
        IdOutput output = objectFactory.createIdOutput();
        if (StringUtils.isNotBlank(input.getPokemon())) {
        	log.info("----> " + input.getPokemon() + "!!!");
        	int id = service.getId(input.getPokemon());
        	output.setId(id);
        }
        return output;
	}
	
	/**
	 * Pokemon Local Area Encounters.
	 * @param input Pokemon nombre.
	 * @return Local Area Encounters.
	 */
	@ResponsePayload
    @PayloadRoot(namespace = "http://pokemon.bankaya.com/types/pokemon", localPart = "LocalAreaEncountersInput")
	public LocalAreaEncountersOutput localAreaEncounters(@RequestPayload LocalAreaEncountersInput input) {
		log.info("--> Pokemon Local Area Encounters");
		saveLog(METHOD_LOCATION_AREA_ENCOUNTERS);
        ObjectFactory objectFactory = new ObjectFactory();
        LocalAreaEncountersOutput output = objectFactory.createLocalAreaEncountersOutput();
        if (StringUtils.isNotBlank(input.getPokemon())) {
        	log.info("----> " + input.getPokemon() + "!!!");
        	String location = service.getLocationAreaEncounters(input.getPokemon());
        	output.setLocalAreaEncounters(location);
        }
        return output;
	}
	
	/**
	 * Pokemon Held Items.
	 * @param input Pokemon nombre.
	 * @return Held Items.
	 */
	@ResponsePayload
    @PayloadRoot(namespace = "http://pokemon.bankaya.com/types/pokemon", localPart = "HeldItemsInput")
	public HeldItemsOutput heldItems(@RequestPayload HeldItemsInput input) {
		log.info("--> Pokemon Local Area Encounters");
		saveLog(METHOD_HELD_ITEMS);
        ObjectFactory objectFactory = new ObjectFactory();
        HeldItemsOutput output = objectFactory.createHeldItemsOutput();
        if (StringUtils.isNotBlank(input.getPokemon())) {
        	log.info("----> " + input.getPokemon() + "!!!");
        	
        	List<PokemonHeldItem> heldItems = service.getHeldItems(input.getPokemon());
			log.info("----> HeldItems: " + heldItems);
			List<HeldItem> heldItemOutput = output.getHeldItems();
	        for(PokemonHeldItem heldItemPoke : heldItems) {
	        	HeldItem heldItem = new HeldItem();
	        	heldItem.setItem(convertItem(heldItemPoke));
	        	List<VersionDetail> versionDetails = heldItem.getVersionDetails();
	        	List<VersionDetail> versionDetailConvert = convertVersionDetails(heldItemPoke);
	        	for (VersionDetail versionDetail : versionDetailConvert) {
	        		versionDetails.add(versionDetail);
	        	}
	        	heldItemOutput.add(heldItem);
	        }
        }
        return output;
	}
	
	/**
	 * Save Log.
	 * @param metodo Nombre metodo.
	 */
	private void saveLog(final String metodo) {
		String ip = request.getRemoteAddr();
		LogEntity entity = LogEntity.builder().fecha(new Date())
											.ip(ip)
											.metodo(metodo)
											.build();
		logRepository.saveAndFlush(entity);
	}
	
	/**
	 * Convert Pokemon Item to Item.
	 * @param pokemonHeldItem Pokemon Held Item.
	 * @return Item.
	 */
	private Item convertItem(final PokemonHeldItem pokemonHeldItem) {
		String itemJson = new Gson().toJson(pokemonHeldItem.getItem());
		log.info("ItemJson: " + itemJson);
		return new Gson().fromJson(itemJson, Item.class);
	}
	
	/**
	 * Convert Pokemon Version Details to Version Details.
	 * @param pokemonHeldItem Pokemon Held Item.
	 * @return Version Details.
	 */
	private List<VersionDetail> convertVersionDetails(final PokemonHeldItem pokemonHeldItem) {
		String versionDetailsJson = new Gson().toJson(pokemonHeldItem.getVersion_details());
		log.info("VersionDetailsJson: " + versionDetailsJson);
		Type heldItemListType = new TypeToken<List<VersionDetail>>(){}.getType();
		return new Gson().fromJson(versionDetailsJson, heldItemListType);
	}

}
