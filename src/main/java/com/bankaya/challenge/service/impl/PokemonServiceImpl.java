package com.bankaya.challenge.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bankaya.challenge.model.PokemonAbility;
import com.bankaya.challenge.model.PokemonHeldItem;
import com.bankaya.challenge.model.PokemonObject;
import com.bankaya.challenge.service.PokemonService;
import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;

/**
 * Implementacion Pokemon Service.
 * @author Antonio Bravo.
 *
 */
@Service
@Slf4j
public class PokemonServiceImpl implements PokemonService {
	
	/** URL POKEAPI */
	private final String POKE_URL = "https://pokeapi.co/api/v2/pokemon/{pokemon}/";
	
	/**
	 * RestTemplate.
	 */
	@Autowired
	private RestTemplate restTemplate;

	/**
     * {@inheritDoc}
     */
	@Override
	public List<PokemonAbility> getAbilities(final String pokemon) {
		log.info("## [Service] getAbilities");
		log.info(POKE_URL);
		PokemonObject pokemonObject = getPokemonObject(pokemon);
		return pokemonObject.getAbilities();
	}
	
	/**
     * {@inheritDoc}
     */
	@Override
	public int getBaseExperience(final String pokemon) {
		log.info("## [Service] getBaseExperience");
		log.info(POKE_URL);
		PokemonObject pokemonObject = getPokemonObject(pokemon);
	    return pokemonObject.getBase_experience();
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public List<PokemonHeldItem> getHeldItems(final String pokemon) {
		log.info("## [Service] getHeldItems");
		log.info(POKE_URL);
		PokemonObject pokemonObject = getPokemonObject(pokemon);
		return pokemonObject.getHeld_items();
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public int getId(final String pokemon) {
		log.info("## [Service] getId");
		log.info(POKE_URL);
		PokemonObject pokemonObject = getPokemonObject(pokemon);
		return pokemonObject.getId();
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public String getName(final String pokemon) {
		log.info("## [Service] getId");
		log.info(POKE_URL);
		PokemonObject pokemonObject = getPokemonObject(pokemon);
		return pokemonObject.getName();
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public String getLocationAreaEncounters(final String pokemon) {
		log.info("## [Service] getId");
		log.info(POKE_URL);
	    PokemonObject pokemonObject = getPokemonObject(pokemon);     
		return pokemonObject.getLocation_area_encounters();
	}
	
	/**
	 * Llamado url pokemon.
	 * @param pokemon Nombre Pokemon.
	 * @return Pokemon.
	 */
	private PokemonObject getPokemonObject(final String pokemon) {
		ResponseEntity<String> response = restTemplate.getForEntity(POKE_URL, String.class, pokemon);
	    return new Gson().fromJson(response.getBody(), PokemonObject.class);
	}

}
