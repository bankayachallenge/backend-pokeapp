package com.bankaya.challenge.service;

import java.util.List;

import com.bankaya.challenge.model.PokemonAbility;
import com.bankaya.challenge.model.PokemonHeldItem;

/**
 * Interface Pokemon.
 * @author Antonio Bravo.
 *
 */
public interface PokemonService {
	
	/**
	 * Service Pokemon Abilities.
	 * @param pokemon Nombre pokemon.
	 * @return Pokemon Abilities.
	 */
	List<PokemonAbility> getAbilities(String pokemon);
	/**
	 * Service Pokemon Base Experience.
	 * @param pokemon Nombre pokemon.
	 * @return Base experience.
	 */
	int getBaseExperience(String pokemon);
	/**
	 * Service Pokemon Held Items.
	 * @param pokemon Nombre pokemon.
	 * @return Held items.
	 */
	List<PokemonHeldItem> getHeldItems(String pokemon);
	/**
	 * Service Pokemon Id.
	 * @param pokemon Nombre pokemon.
	 * @return Id.
	 */
	int getId(String pokemon);
	/**
	 * Service Pokemon Name.
	 * @param pokemon Nombre pokemon.
	 * @return Name.
	 */
	String getName(String pokemon);
	/**
	 * Service Pokemon Location Area Encounters.
	 * @param pokemon Nombre pokemon.
	 * @return Local area encounters.
	 */
	String getLocationAreaEncounters(String pokemon);

}
