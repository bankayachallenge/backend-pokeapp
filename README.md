# PokeApp

Challenge Backend Bankaya

## Getting started

Se necesita minimo Java 8 y Maven 3 instalados.

Ejecutamos la aplicacion:
- mvn clean install
- mvn spring-boot:run

Identificamos el WSDL que se encuentra en src/main/resources/pokemon.wsdl
- Cargamos el archivo en SOAP UI

Lanzamos las peticiones desde SOAP UI
- abilities
- baseExperience
- heldItems
- id
- localAreaEncounters
- name

Revisamos la base de datos http://localhost:8080/h2-ui
- select * from log;


